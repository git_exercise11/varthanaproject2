import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage-angular';
import { Location } from '@angular/common';
@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.page.html',
  styleUrls: ['./dash-board.page.scss'],
})
export class DashBoardPage implements OnInit {
  constructor(
    private router: Router,
    private storage: Storage,
    private location: Location
  ) {}
  ngOnInit() {
    this.storage.get('isLoggedIn').then((isLoggedIn) => {
      if (!isLoggedIn) {
        this.router.navigate(['/login']);
      }
    });
  }

  showWeeklyPieChart() {
    this.router.navigate(['/sign-up-details']);
  }

  showFetchedData() {
    this.router.navigate(['/api-fetch']);
  }

  showLoanDetails() {
    this.router.navigate(['/loan-details']);
  }

  navigateToLogin() {
    this.router.navigate(['']);
  }

  navigateBack() {
    this.location.back();
  }

  async logout() {
    await this.storage.remove('isLoggedIn');
    this.router.navigate(['/login']);
  }
}

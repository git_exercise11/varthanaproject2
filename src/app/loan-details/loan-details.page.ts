import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoanService } from '../loan.service';
@Component({
  selector: 'app-loan-details',
  templateUrl: './loan-details.page.html',
  styleUrls: ['./loan-details.page.scss'],
})
export class LoanDetailsPage implements OnInit {
  data: any;
  token!: string;

  constructor(private loanService: LoanService, private http: HttpClient) {}

  ngOnInit() {
    this.loanService.generateToken(41, 'current').subscribe((response) => {
      this.token = response.token;

      this.fetchData();
    });
  }

  fetchData() {
    console.log('token-------', this.token);
    const apiUrl = 'http://65.2.107.120:8080/rmvarthanaloan/getloanhistory';
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.token}`,
    });
    this.http.get(apiUrl, { headers }).subscribe((data) => {
      this.data = data;
    });
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoanService {
  private apiUrl = 'http://localhost:3000/generate-token';

  constructor(private http: HttpClient) {}

  generateToken(RmLoginid: number, filter: string): Observable<any> {
    return this.http.post<any>(this.apiUrl, { RmLoginid, filter });
  }
}

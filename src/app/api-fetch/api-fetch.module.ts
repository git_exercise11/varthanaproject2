import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ApiFetchPageRoutingModule } from './api-fetch-routing.module';

import { ApiFetchPage } from './api-fetch.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ApiFetchPageRoutingModule
  ],
  declarations: [ApiFetchPage]
})
export class ApiFetchPageModule {}

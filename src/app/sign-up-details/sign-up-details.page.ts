import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { Location } from '@angular/common';
import Chart from 'chart.js/auto';

@Component({
  selector: 'app-sign-up-details',
  templateUrl: './sign-up-details.page.html',
  styleUrls: ['./sign-up-details.page.scss'],
})
export class SignUpDetailsPage implements OnInit {
  todaySignups: any[] = [];
  lastTwoDaysSignups: any[] = [];
  weeklySignups: Record<string, number> = {};
  today = new Date().toISOString().split('T')[0];

  constructor(private storage: Storage, private location: Location) {}

  navigateToDashBoard() {
    this.location.back();
  }
  async ngOnInit() {
    await this.loadSignups();
    this.generatePieChart(
      'signupChartToday',
      this.countSignupsByEmail(this.todaySignups)
    );
    this.generatePieChart('signupChartWeekly', this.weeklySignups);
  }

  async loadSignups() {
    this.todaySignups = await this.getTodaySignups();
    this.lastTwoDaysSignups = await this.getLastTwoDaysSignups();
    this.weeklySignups = await this.getWeeklySignups();
  }

  async getTodaySignups(): Promise<any[]> {
    const keys = await this.storage.keys();
    const signups: any[] = [];

    for (const key of keys) {
      if (key.startsWith('userData_')) {
        const userData = await this.storage.get(key);
        const signupDate = new Date(userData.date).toISOString().split('T')[0];
        if (signupDate === this.today) {
          signups.push(userData);
        }
      }
    }

    return signups;
  }

  async getLastTwoDaysSignups(): Promise<any[]> {
    const today = new Date();
    const yesterday = new Date(today);
    yesterday.setDate(today.getDate() - 1);

    const todayString = today.toISOString().split('T')[0];
    const yesterdayString = yesterday.toISOString().split('T')[0];

    const keys = await this.storage.keys();
    const signups: any[] = [];

    for (const key of keys) {
      if (key.startsWith('userData_')) {
        const userData = await this.storage.get(key);
        const signupDate = new Date(userData.date).toISOString().split('T')[0];
        if (signupDate === todayString || signupDate === yesterdayString) {
          signups.push(userData);
        }
      }
    }

    return signups;
  }

  async getWeeklySignups(): Promise<Record<string, number>> {
    const keys = await this.storage.keys();
    const signupData: Record<string, number> = {};

    for (const key of keys) {
      if (key.startsWith('userData_')) {
        const userData = await this.storage.get(key);
        const signupDate = new Date(userData.date);
        const weekStart = this.getWeekStart(signupDate);
        const weekStartISO = weekStart.toISOString().split('T')[0];

        signupData[weekStartISO] = (signupData[weekStartISO] || 0) + 1;
      }
    }

    return signupData;
  }

  getWeekStart(date: Date): Date {
    const weekStart = new Date(date);
    weekStart.setHours(0, 0, 0, 0);
    weekStart.setDate(weekStart.getDate() - weekStart.getDay());
    return weekStart;
  }

  countSignupsByEmail(signups: any[]): Record<string, number> {
    const signupData: Record<string, number> = {};

    for (const signup of signups) {
      const email = signup.email;
      signupData[email] = (signupData[email] || 0) + 1;
    }

    return signupData;
  }

  generatePieChart(chartId: string, signupData: Record<string, number>) {
    const labels = Object.keys(signupData);
    const data = Object.values(signupData);

    new Chart(chartId, {
      type: 'pie',
      data: {
        labels: labels,
        datasets: [
          {
            data: data,
            backgroundColor: [
              '#FF6384',
              '#36A2EB',
              '#FFCE56',
              '#8BC34A',
              '#9C27B0',
              '#795548',
            ],
          },
        ],
      },
    });
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SignUpDetailsPage } from './sign-up-details.page';

describe('SignUpDetailsPage', () => {
  let component: SignUpDetailsPage;
  let fixture: ComponentFixture<SignUpDetailsPage>;

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpDetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

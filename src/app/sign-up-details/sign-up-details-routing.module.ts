import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignUpDetailsPage } from './sign-up-details.page';

const routes: Routes = [
  {
    path: '',
    component: SignUpDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SignUpDetailsPageRoutingModule {}
